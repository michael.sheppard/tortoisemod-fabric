/*
 * TortoiseEntityRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.tortoisemod.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.tortoisemod.common.TortoiseEntity;
import net.tortoisemod.common.TortoiseMod;

@Environment(EnvType.CLIENT)
public class TortoiseEntityRenderer extends MobEntityRenderer<TortoiseEntity, TortoiseEntityModel<TortoiseEntity>> {
    private static final Identifier SKIN = new Identifier(TortoiseMod.MODID, "textures/entity/tortoise/tortoise.png");

    public TortoiseEntityRenderer(EntityRendererFactory.Context context) {
        super(context, new TortoiseEntityModel<>(context.getPart(TortoiseModClient.tortoiseModelLayer)), 0.0f);
    }

    @Override
    protected void scale(TortoiseEntity tortoiseEntity, MatrixStack matrixStack, float unknown) {
        float scaleFactor = tortoiseEntity.getScaleFactor();
        matrixStack.scale(scaleFactor, scaleFactor, scaleFactor);
    }

    @Override
    public Identifier getTexture(TortoiseEntity entity) {
        return SKIN;
    }
}
