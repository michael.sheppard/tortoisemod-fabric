/*
 * TortoiseEntityModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.tortoisemod.client;

import com.google.common.collect.ImmutableList;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.*;
import net.minecraft.client.render.entity.model.QuadrupedEntityModel;
import net.minecraft.util.math.MathHelper;
import net.tortoisemod.common.TortoiseEntity;

@Environment(EnvType.CLIENT)
public class TortoiseEntityModel<T extends TortoiseEntity> extends QuadrupedEntityModel<T> {
    private static ModelPart tail;
    private static ModelPart plastron;

    public TortoiseEntityModel(ModelPart root) {
        super(root, false, 120.0F, 0.0F, 9.0F, 6.0F, 120);
    }

    public static TexturedModelData getTexturedModelData() {
        ModelData modelData = new ModelData();
        ModelPartData modelPartData = modelData.getRoot();
        float yPos = 22F;

        modelPartData.addChild("head", ModelPartBuilder.create().uv(0, 0).cuboid(-2F, 0F, -4F, 4, 3, 4), ModelTransform.pivot(0F, yPos - 3, -4F));

        modelPartData.addChild("body", ModelPartBuilder.create().uv(0, 23).cuboid(-3F, 0F, -3F, 6, 3, 6), ModelTransform.pivot(0F, yPos - 4, 0F));

        modelPartData.addChild("right_hind_leg", ModelPartBuilder.create().uv(60, 0).cuboid(-1F, 0F, 0F, 1, 3, 1)
                .mirrored(), ModelTransform.of(-2F, yPos, 2F, 0f, 0f, 0.7853981633974483F));
        modelPartData.addChild("left_hind_leg", ModelPartBuilder.create().uv(60, 0).cuboid(0F, 0F, 0F, 1, 3, 1),
                ModelTransform.of(2F, yPos, 2F, 0f, 0f, 5.497787143782138F));
        modelPartData.addChild("right_front_leg", ModelPartBuilder.create().uv(60, 0).cuboid(-1F, 0F, 0F, 1, 3, 1)
                .mirrored(), ModelTransform.of(-2F, yPos, -3F, 0f, 0f, 0.7853981633974483F));
        modelPartData.addChild("left_front_leg", ModelPartBuilder.create().uv(60, 0).cuboid(0F, 0F, 0F, 1, 3, 1),
                ModelTransform.of(2F, yPos, -3F, 0f, 0f, 5.497787143782138F));

        ModelPartBuilder tailPartBuilder = ModelPartBuilder.create().uv(58, 29).cuboid(0F, 0F, 0F, 1, 1, 2);
        modelPartData.addChild("tail", tailPartBuilder, ModelTransform.pivot(-0.5F, yPos - 1, 4F));
        tail = modelPartData.getChild("tail").createPart(64, 32);

        ModelPartBuilder plastronBuilder = ModelPartBuilder.create().uv(24, 23).cuboid(-4F, -1F, -4F, 8, 1, 8);
        modelPartData.addChild("plastron", plastronBuilder, ModelTransform.pivot(0F, yPos, 0F));
        plastron = modelPartData.getChild("plastron").createPart(64, 32);

        return TexturedModelData.of(modelData, 64, 32);
    }

    @Override
    public void setAngles(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        head.pitch = headPitch * 0.0174532924F;
        head.yaw = netHeadYaw * 0.0174532924F;

        leftFrontLeg.pitch = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        leftHindLeg.pitch = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        rightFrontLeg.pitch = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        rightHindLeg.pitch = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        tail.pitch = 5.934119456780721F;
        tail.yaw = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    public Iterable<ModelPart> getBodyParts() {
        return ImmutableList.of(body, head, leftFrontLeg, leftHindLeg, rightFrontLeg, rightHindLeg, plastron, tail);
    }
}
