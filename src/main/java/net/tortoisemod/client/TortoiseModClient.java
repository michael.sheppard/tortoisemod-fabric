/*
 * TortoisemodClient.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.tortoisemod.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.util.Identifier;
import net.tortoisemod.common.TortoiseMod;

@Environment(EnvType.CLIENT)
public class TortoiseModClient implements ClientModInitializer {
    public static final EntityModelLayer tortoiseModelLayer = new EntityModelLayer(new Identifier(TortoiseMod.MODID, "textures/entity/tortoise/tortoise.png"), TortoiseMod.TORTOISE_NAME);
    @Override
    public void onInitializeClient() {
        EntityModelLayerRegistry.registerModelLayer(tortoiseModelLayer, TortoiseEntityModel::getTexturedModelData);
        EntityRendererRegistry.INSTANCE.register(TortoiseMod.TORTOISE, TortoiseEntityRenderer::new);
    }
}
