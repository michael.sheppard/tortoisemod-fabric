/*
 * Tortoisemod.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.tortoisemod.common;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class TortoiseMod implements ModInitializer {
    public static final String MODID = "tortoisemod";
    public static final String TORTOISE_NAME = "tortoise";
    public static final String TORTOISE_SPAWN_EGG = "tortoise_spawn_egg";

    public static final EntityType<TortoiseEntity> TORTOISE = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(TortoiseMod.MODID, TORTOISE_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, TortoiseEntity::new).dimensions(EntityDimensions.fixed(1.0f, 2.5f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    @Override
    public void onInitialize() {
        FabricDefaultAttributeRegistry.register(TortoiseMod.TORTOISE, TortoiseEntity.createAttributes());
        Registry.register(Registry.ITEM, new Identifier(TortoiseMod.MODID, TortoiseMod.TORTOISE_SPAWN_EGG),
                new SpawnEggItem(TORTOISE, 0x8B4513, 0x8B4C39, new Item.Settings().group(ItemGroup.MISC)));
    }
}
