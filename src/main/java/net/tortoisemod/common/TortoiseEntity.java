/*
 * DesertTortoiseEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.tortoisemod.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;

public class TortoiseEntity extends AnimalEntity {
    public EntityDimensions turtleSize = new EntityDimensions(1.0f, 0.3f, false);

    private final float WIDTH = 1.0f;
    private final float HEIGHT = 0.3f;
    private final float scaleFactor;

    protected TortoiseEntity(EntityType<? extends TortoiseEntity> entityType, World world) {
        super(entityType, world);
        float scale = random.nextFloat();
        scaleFactor = scale < 0.6F ? 1.0F : scale;
        turtleSize.scaled(WIDTH * getScaleFactor(), HEIGHT * getScaleFactor());
    }

    public static DefaultAttributeContainer.Builder createAttributes() {
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.20)
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 8.0D);
    }

    @Override
    protected void initGoals() {
        goalSelector.add(1, new SwimGoal(this));
        goalSelector.add(2, new WanderAroundFarGoal(this, 1.0D));
        goalSelector.add(3, new LookAtEntityGoal(this, PlayerEntity.class, 8.0F));
        goalSelector.add(4, new LookAroundGoal(this));
    }

    @SuppressWarnings("unused")
    // implementing Biome specific spawning here
    public static boolean canSpawn(EntityType<TortoiseEntity> type, WorldAccess world, SpawnReason spawnReason, BlockPos pos, Random random) {
        return validSpawnBiomes(world, pos);
    }

    private static boolean validSpawnBiomes(WorldAccess world, BlockPos pos) {
        Optional<RegistryKey<Biome>> optional = world.getBiomeKey(pos);
        return Objects.equals(optional, Optional.of(BiomeKeys.SAVANNA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.DESERT)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.DESERT_HILLS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.DESERT_LAKES));
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    @Override
    public EntityDimensions getDimensions(EntityPose p) {
        return new EntityDimensions(WIDTH, HEIGHT, false);
    }

    @Override
    protected float getActiveEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return dimensions.height * 0.9F;
    }

    @Override
    protected void playStepSound(BlockPos pos, BlockState blockIn) {
        playSound(getStepSound(), 0.15F, 1.0F);
    }

    protected SoundEvent getStepSound() {
        return SoundEvents.ENTITY_CHICKEN_STEP;
    }

    @Override
    protected float getSoundVolume() {
        return 0.4F;
    }

    @Override
    public @Nullable PassiveEntity createChild(ServerWorld world, PassiveEntity entity) {
        return TortoiseMod.TORTOISE.create(world);
    }
}
